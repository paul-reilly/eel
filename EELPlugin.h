#ifndef __EELPLUGIN__
#define __EELPLUGIN__


#include "EELVM.h"
#include "IPlug_include_in_plug_hdr.h"
#include "IMidiQueue.h"
#include "IControl.h"


class EELPlugin
{
public:
	enum section {
		kInit = 0,
		kSlider,
		kBlock,
		kSample
	};

	EELPlugin(const char* fn) : mEVM({ "@init", "@slider", "@block", "@sample" }, fn)
	{
		mEVM.registerFunction("midirecv", 3, &EELPlugin::midiRecv);
		mEVM.registerFunction("midisend", 3, &EELPlugin::midiSend);

		srate = mEVM.registerVar("srate");
		samplesblock = mEVM.registerVar("samplesblock");
		spl0 = mEVM.registerVar("spl0");
		spl1 = mEVM.registerVar("spl1");

		slider1 = mEVM.registerVar("slider1");
		slider2 = mEVM.registerVar("slider2");
		slider3 = mEVM.registerVar("slider3");
		slider4 = mEVM.registerVar("slider4");
		slider5 = mEVM.registerVar("slider5");
		slider6 = mEVM.registerVar("slider6");
		slider7 = mEVM.registerVar("slider7");
		slider8 = mEVM.registerVar("slider8");
		slider9 = mEVM.registerVar("slider9");
		slider10 = mEVM.registerVar("slider10");

		mEVM.setThis(this);
		mEVM.compileFile();
		m_codehandles = mEVM.getCodeHandles();		
	}

	~EELPlugin() {}

	virtual void onInit() { mEVM.executeHandle(section::kInit); };
	virtual void onSlider() { mEVM.executeHandle(section::kSlider); };

	virtual void onBlock() 
	{ 
		mEVM.executeHandle(section::kBlock);
		while (!mTempQueue.Empty())
		{
			mMidiQueue->Add(mTempQueue.Peek());
			mTempQueue.Remove();
		} 
		mTempQueue.Empty();
	};


	virtual void onSample(double* left, double* right) 
	{
		spl0 = left; spl1 = right;
		mEVM.executeHandle(section::kSample); 
	};


	void onReset(int sr, int bs)
	{
		*srate = sr*1000;
		*samplesblock = bs;
		onInit();
	}


	void setMidiQueue(IMidiQueue &imq) { 
		mMidiQueue = &imq;
	};
	

	static EEL_F NSEEL_CGEN_CALL midiSend(void *ctx, INT_PTR nop, EEL_F **params)
	{
		IMidiMsg msg;
		msg.mOffset = (int)*params[0];
		msg.mStatus = (BYTE)*params[1];
		msg.mData1 = (BYTE)*params[2];
		msg.mData2 = (BYTE)*params[3];
		static_cast<EELPlugin*>(ctx)->mTempQueue.Add(&msg);
		return 1.;
	}


	static EEL_F NSEEL_CGEN_CALL midiRecv(void *ctx, INT_PTR nop, EEL_F **params)
	{   
		if (static_cast<EELPlugin*>(ctx)->mMidiQueue->Empty()) { *params[0] = -1; return NULL; };

		IMidiMsg* msg = static_cast<EELPlugin*>(ctx)->mMidiQueue->Peek();
		*params[0] = msg->mOffset;
		*params[1] = msg->mStatus;
		*params[2] = msg->mData1;
		*params[3] = msg->mData2;
		static_cast<EELPlugin*>(ctx)->mMidiQueue->Remove();
		return 1.;
	}

private:
	EELVM mEVM;
	double *samplesblock, *srate;
	double *spl0, *spl1;
	double *slider1, *slider2, *slider3, *slider4, *slider5;
	double *slider6, *slider7, *slider8, *slider9, *slider10;
	IMidiQueue* mMidiQueue;
	IMidiQueue mTempQueue;
	WDL_PtrKeyedArray<NSEEL_CODEHANDLE>* m_codehandles;
};



#endif
#pragma once
/*--------------------------------------------------------------
EELVM.h

	manager class for instance of EEL Virtual Machine

	Example use:

	EELVM evm = new EELVM(this, {"@init", "@sample"}, "filename.ext");
	evm->compileFile();

	Now if the sections, @init and @sample compiled correctly, they
	will be stored in a zero base index. So to execute @sample...

	evm->executeHandle(1);


--------------------------------------------------------------*/


#define EELVM_MAX_ALG_LENGTH 65536

#include "../../WDL/eel2/ns-eel.h"
#include "../../WDL/eel2/ns-eel-addfuncs.h"
#include "../../WDL/lineparse.h"
#include "../../WDL/wdlcstring.h"
#include "../../WDL/dirscan.h"
#include "../../WDL/queue.h"
#include "../../WDL/assocarray.h"
#include <vector>
#include <string>
#include "Log.h"


class EELVM
{
public:
	EELVM(std::vector<const char*> sections);
	EELVM(void* this_ptr, std::vector<const char*> sections, const char* filename);
	EELVM(std::vector<const char*> sections, const char* filename);
	~EELVM();

	// fptr is pointer to function that must have those arguments
	void registerFunction(const char* name, int min_arguments, EEL_F(NSEEL_CGEN_CALL *fptr)(void *, INT_PTR, EEL_F **));
	double* registerVar(const char* name);
	bool executeHandle(int h);
	void setThis(void* t);
	WDL_PtrKeyedArray<NSEEL_CODEHANDLE>* getCodeHandles();
	NSEEL_VMCTX getVM() { return VM; };
	void compileFile();

	
private:
	void SetCodeSection(const char* tok,int parsestate, const WDL_FastString &curblock, WDL_FastString &results, int lineoffs);
	NSEEL_VMCTX VM;
	WDL_PtrKeyedArray<NSEEL_CODEHANDLE> m_codehandles;
	WDL_FastString m_filename;
	std::vector<const char*> m_code_names;
};
